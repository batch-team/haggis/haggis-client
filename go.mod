module gitlab.cern.ch/batch-team/haggis-client

require (
	github.com/olekukonko/tablewriter v0.0.1
	github.com/spf13/cobra v1.8.0
	gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-entities v1.0.0
	gitlab.cern.ch/batch-team/negotiate v1.2.0
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/hashicorp/go-uuid v1.0.3 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/jcmturner/aescts/v2 v2.0.0 // indirect
	github.com/jcmturner/dnsutils/v2 v2.0.0 // indirect
	github.com/jcmturner/gofork v1.7.6 // indirect
	github.com/jcmturner/goidentity/v6 v6.0.1 // indirect
	github.com/jcmturner/gokrb5/v8 v8.4.4 // indirect
	github.com/jcmturner/rpc/v2 v2.0.3 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.1-0.20181226105442-5d4384ee4fb2 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/net v0.20.0 // indirect
)

replace gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-entities => gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-entities.git v1.0.0

go 1.19
