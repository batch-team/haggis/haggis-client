package commands

import (
	"fmt"

	"github.com/spf13/cobra"
)

const membersEndpoint = "mapping/groups"

var membersCmd = &cobra.Command{
	Use:   "members [group]",
	Short: "Display the members of an accounting group",
	Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		name := args[0]
		endpoint := fmt.Sprintf("%s/%s", membersEndpoint, name)
		ret, err := client.Get(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		members := []string{}
		membersHeader := []string{name}

		codec.Decode(ret, &members)

		for _, member := range members {
			line := []string{member}
			printer.Append(line)
		}

		printer.Print(membersHeader)

		return nil
	},
}

func init() {
	rootCmd.AddCommand(membersCmd)
}
