package commands

import (
	entities "gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-entities"
	"strconv"

	"github.com/spf13/cobra"
)

const propertiesEndpoint = "chargeproperties"

var propertiesHeader = []string{"ID", "Pledge", "Type"}

var propertiesCmd = &cobra.Command{
	Use:   "properties",
	Short: "Read the available properties properties in the system",
}

var propertiesListCmd = &cobra.Command{
	Use:   "list",
	Short: "Read all the available properties properties in the system",

	RunE: func(cmd *cobra.Command, args []string) error {
		ret, err := client.Get(propertiesEndpoint)

		if err != nil {
			return handleError(ret, err)
		}

		propertiess := make([]entities.ChargeProperties, 0)
		codec.Decode(ret, &propertiess)

		for _, property := range propertiess {
			line := []string{strconv.Itoa(property.ID), strconv.FormatBool(property.Pledge), property.Type}
			printer.Append(line)
		}

		printer.Print(propertiesHeader)

		return nil
	},
}

func init() {
	propertiesCmd.AddCommand(propertiesListCmd)

	rootCmd.AddCommand(propertiesCmd)
}
