package commands

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.cern.ch/batch-team/haggis-client/haggis"
	"gitlab.cern.ch/batch-team/haggis-client/json"
	"gitlab.cern.ch/batch-team/haggis-client/table"
)

var rootCmd = &cobra.Command{
	Use: "haggis",

	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		fmt.Println("")

		scheme, _ := cmd.Flags().GetString("scheme")
		host, _ := cmd.Flags().GetString("host")
		port, _ := cmd.Flags().GetString("port")
		user, _ := cmd.Flags().GetString("user")
		negotiate, _ := cmd.Flags().GetBool("negotiate")

		client = haggis.New(scheme, host, port, user, negotiate)
		codec = json.New()
		printer = table.New(os.Stdout)

		return nil
	},

	SilenceErrors: true,
	SilenceUsage:  true,
}

//Execute initializes and runs the root command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		message := fmt.Sprintf("Error during command execution: %v.", err)
		fmt.Println(message)
	}
}

func init() {
	rootCmd.PersistentFlags().StringP("host", "H", "haggis02.cern.ch", "Haggis backend server host")
	rootCmd.PersistentFlags().StringP("port", "p", "8204", "Haggis backend server host")
	rootCmd.PersistentFlags().StringP("scheme", "", "https", "http/https")
	rootCmd.PersistentFlags().Bool("negotiate", true, "Authenticate via kerberos negotiation")
	rootCmd.PersistentFlags().StringP("user", "u", "", "Only for testing. Note that this will be overwritten if the negotiate flag is not set to false")
}
