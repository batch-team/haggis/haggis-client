package commands

import (
	"errors"
	"fmt"
	entities "gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-entities"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
)

const computeEndpoint = "compute"

var computeHeader = []string{"ID", "Name", "Type", "Mapped To"}

type compute struct {
	Name       string `json:"name"`
	Type       string `json:"type"`
	Accounting int    `json:"accounting"`
}

var computeCmd = &cobra.Command{
	Use:   "compute",
	Short: "Manage compute groups",
}

var computeCreateCmd = &cobra.Command{
	Use:   "create [name] [type] [accounting]",
	Short: "Create new compute groups",
	Args:  cobra.ExactArgs(3),

	RunE: func(cmd *cobra.Command, args []string) error {
		name := args[0]
		groupType := args[1]
		accounting, _ := strconv.Atoi(args[2])

		c := compute{
			Name:       name,
			Type:       groupType,
			Accounting: accounting,
		}

		data, _ := codec.Encode(c)
		ret, err := client.Post(computeEndpoint, data)

		if err != nil {
			return handleError(ret, err)
		}

		group := entities.ComputeGroup{}
		codec.Decode(ret, &group)

		fmt.Println("New resource:")
		line := []string{strconv.Itoa(group.ID), group.Name, group.Type, strconv.Itoa(group.Accounting)}
		printer.Append(line)
		printer.Print(computeHeader)

		return nil
	},
}

var computeListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all compute groups in the system",

	RunE: func(cmd *cobra.Command, args []string) error {
		ret, err := client.Get(computeEndpoint)

		if err != nil {
			return handleError(ret, err)
		}

		groups := make([]entities.ComputeGroupExtended, 0)
		codec.Decode(ret, &groups)

		for _, group := range groups {
			line := []string{strconv.Itoa(group.Group.ID), group.Group.Name, group.Group.Type, group.MappedTo.Name}
			printer.Append(line)
		}

		printer.Print(computeHeader)

		return nil
	},
}

var computeGetCmd = &cobra.Command{
	Use:   "get [compute group 'name' or 'ID'] or [accounting group 'name']",
	Short: "Get a specific compute group",
	Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]

		endpoint := fmt.Sprintf("%s/%s", computeEndpoint, id)

		if strings.Contains(id, "group_u_") {
			endpoint = fmt.Sprintf("%s/reverse/%s", computeEndpoint, id)
		}

		ret, err := client.Get(endpoint)
		fmt.Println(string(ret))

		if err != nil {
			return handleError(ret, err)
		}

		var groups []entities.ComputeGroupExtended
		err = codec.Decode(ret, &groups)
		if err != nil {
			return handleError(ret, err)
		}

		for _, group := range groups {
			printer.Append([]string{strconv.Itoa(group.Group.ID), group.Group.Name, group.Group.Type, group.MappedTo.Name})
		}
		printer.Print(computeHeader)
		return nil
	},
}

var computeUpdateCmd = &cobra.Command{
	Use:   "update [id] [field] [value]",
	Short: "Update a compute group",
	Long:  `Update a compute group. Only the mapped accounting group is updateable.`,
	Args:  cobra.ExactArgs(3),

	PreRunE: func(cmd *cobra.Command, args []string) error {
		if args[1] != "accounting" {
			return errors.New("only the accounting field is updatable")
		}

		return nil
	},

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]
		field := args[1]
		value := args[2]

		endpoint := fmt.Sprintf("%s/%s/%s", computeEndpoint, field, id)
		ret, err := client.Put(endpoint, nil, value)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

var computeDeleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Remove a compute group from the system",
	Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]

		endpoint := fmt.Sprintf("%s/%s", computeEndpoint, id)
		ret, err := client.Delete(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

func init() {
	computeCmd.AddCommand(computeCreateCmd)
	computeCmd.AddCommand(computeListCmd)
	computeCmd.AddCommand(computeGetCmd)
	computeCmd.AddCommand(computeUpdateCmd)
	computeCmd.AddCommand(computeDeleteCmd)

	rootCmd.AddCommand(computeCmd)
}
