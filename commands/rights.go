package commands

import (
	"fmt"
	"os/user"

	"github.com/spf13/cobra"
)

const rightsEndpoint = "mapping/users"

var rightsCmd = &cobra.Command{
	Use:   "rights [user]",
	Short: "Display the submission rights for a user",
	//Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		var name string

		if len(args) > 0 {
			name = args[0]
		} else {
			user, _ := user.Current()
			name = user.Username
		}

		endpoint := fmt.Sprintf("%s/%s", rightsEndpoint, name)
		ret, err := client.Get(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		rights := []string{}
		rightsHeader := []string{name}

		codec.Decode(ret, &rights)

		for _, right := range rights {
			line := []string{right}
			printer.Append(line)
		}

		printer.Print(rightsHeader)

		return nil
	},
}

func init() {
	rootCmd.AddCommand(rightsCmd)
}
