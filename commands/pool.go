package commands

import (
	"errors"
	"fmt"
	entities "gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-entities"
	"strconv"

	"github.com/spf13/cobra"
)

const poolEndpoint = "pool"

var poolHeader = []string{"ID", "Name", "Description"}

type pool struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

var poolCmd = &cobra.Command{
	Use:   "pool",
	Short: "Manage resource pools",
}

var poolCreateCmd = &cobra.Command{
	Use:   "create [name] [description]",
	Short: "Create new resource pools",
	Args:  cobra.ExactArgs(2),

	RunE: func(cmd *cobra.Command, args []string) error {
		name := args[0]
		description := args[1]

		p := pool{
			Name:        name,
			Description: description,
		}

		data, _ := codec.Encode(p)
		ret, err := client.Post(poolEndpoint, data)

		if err != nil {
			return handleError(ret, err)
		}

		pool := entities.Pool{}
		codec.Decode(ret, &pool)

		fmt.Println("New resource:")

		line := []string{strconv.Itoa(pool.ID), pool.Name, pool.Description}
		printer.Append(line)
		printer.Print(poolHeader)

		return nil
	},
}

var poolListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all resource pools in the system",

	RunE: func(cmd *cobra.Command, args []string) error {
		ret, err := client.Get(poolEndpoint)

		if err != nil {
			return handleError(ret, err)
		}

		pools := make([]entities.Pool, 0)
		codec.Decode(ret, &pools)

		for _, pool := range pools {
			line := []string{strconv.Itoa(pool.ID), pool.Name, pool.Description}
			printer.Append(line)
		}

		printer.Print(poolHeader)

		return nil
	},
}

var poolGetCmd = &cobra.Command{
	Use:   "get [pool]",
	Short: "Get a specific resource pool",
	Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]
		endpoint := fmt.Sprintf("%s/%s", poolEndpoint, id)

		ret, err := client.Get(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		pool := entities.Pool{}
		codec.Decode(ret, &pool)

		line := []string{strconv.Itoa(pool.ID), pool.Name, pool.Description}
		printer.Append(line)
		printer.Print(poolHeader)

		return nil
	},
}

var poolUpdateCmd = &cobra.Command{
	Use:   "update [id] [field] [value]",
	Short: "Update a resource pool",
	Long:  `Update a resource pool. Only the description field is updatable.`,
	Args:  cobra.ExactArgs(3),

	PreRunE: func(cmd *cobra.Command, args []string) error {
		if args[1] != "description" {
			return errors.New("only the description field is updatable")
		}

		return nil
	},

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]
		field := args[1]
		value := args[2]

		endpoint := fmt.Sprintf("%s/%s/%s", poolEndpoint, field, id)
		ret, err := client.Put(endpoint, nil, value)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

var poolDeleteCmd = &cobra.Command{
	Use:   "delete [pool]",
	Short: "Remove a resource pool from the system",
	Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]

		endpoint := fmt.Sprintf("%s/%s", poolEndpoint, id)
		ret, err := client.Delete(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

func init() {
	poolCmd.AddCommand(poolCreateCmd)
	poolCmd.AddCommand(poolListCmd)
	poolCmd.AddCommand(poolGetCmd)
	poolCmd.AddCommand(poolUpdateCmd)
	poolCmd.AddCommand(poolDeleteCmd)

	rootCmd.AddCommand(poolCmd)
}
