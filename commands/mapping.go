package commands

import (
	"fmt"

	"github.com/spf13/cobra"
)

const mappingEndpoint = "mapping"

var mappingCmd = &cobra.Command{
	Use:   "mapping",
	Short: "Populate and flush the group/user mappings (admin only)",
}

var mappingPopulateCmd = &cobra.Command{
	Use:   "populate",
	Short: "Recreate all mappings (admin only)",

	RunE: func(cmd *cobra.Command, args []string) error {
		ret, err := client.Post(mappingEndpoint, nil)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

var mappingFlushCmd = &cobra.Command{
	Use:   "flush",
	Short: "Drop all mappings (admin only)",

	RunE: func(cmd *cobra.Command, args []string) error {
		ret, err := client.Delete(mappingEndpoint)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

func init() {
	mappingCmd.AddCommand(mappingPopulateCmd)
	mappingCmd.AddCommand(mappingFlushCmd)

	rootCmd.AddCommand(mappingCmd)
}
