package commands

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	entities "gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-entities"
	"strconv"
)

const groupEndpoint = "accounting"

var groupHeader = []string{"ID", "Name", "Description", "Parent", "Charge To"}

type group struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Parent      int    `json:"parent,omitempty"`
	ChargeTo    int    `json:"chargetogroup,omitempty"`
}

var grpupCmd = &cobra.Command{
	Use:   "group",
	Short: "Manage accounting groups",
}

var groupCreateCmd = &cobra.Command{
	Use:   "create [name] [description] [parent]",
	Short: "Create new accounting groups",
	Args:  cobra.ExactArgs(3),

	RunE: func(cmd *cobra.Command, args []string) error {
		name := args[0]
		description := args[1]
		parent, _ := strconv.Atoi(args[2])

		g := group{
			Name:        name,
			Description: description,
			Parent:      parent,
		}

		data, _ := codec.Encode(g)
		ret, err := client.Post(groupEndpoint, data)

		if err != nil {
			return handleError(ret, err)
		}

		acct := entities.AccountingGroup{}
		codec.Decode(ret, &acct)

		fmt.Println("New resource:")

		line := []string{strconv.Itoa(acct.ID), acct.Name, acct.Description, strconv.Itoa(acct.Parent), strconv.Itoa(acct.ChargeToGroup)}
		printer.Append(line)
		printer.Print(groupHeader)

		return nil
	},
}

var groupCreateTopCmd = &cobra.Command{
	Use:   "top [name] [description] [charge group]",
	Short: "Create new accounting top-level groups",
	Args:  cobra.ExactArgs(3),

	RunE: func(cmd *cobra.Command, args []string) error {
		name := args[0]
		description := args[1]
		charge, _ := strconv.Atoi(args[2])

		g := group{
			Name:        name,
			Description: description,
			ChargeTo:    charge,
		}

		endpoint := fmt.Sprintf("%s/top", groupEndpoint)
		data, _ := codec.Encode(g)
		ret, err := client.Post(endpoint, data)

		if err != nil {
			return handleError(ret, err)
		}

		acct := entities.AccountingGroup{}
		codec.Decode(ret, &acct)

		fmt.Println("New resource:")

		line := []string{strconv.Itoa(acct.ID), acct.Name, acct.Description, strconv.Itoa(acct.Parent), strconv.Itoa(acct.ChargeToGroup)}
		printer.Append(line)
		printer.Print(groupHeader)

		return nil
	},
}

var groupListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all accounting groups in the system",

	RunE: func(cmd *cobra.Command, args []string) error {
		ret, err := client.Get(groupEndpoint)

		if err != nil {
			return handleError(ret, err)
		}

		accts := make([]entities.AccountingGroupExtended, 0)
		codec.Decode(ret, &accts)

		for _, acct := range accts {
			line := []string{strconv.Itoa(acct.Group.ID), acct.Group.Name, acct.Group.Description, acct.Parent.Name, acct.ChargeToGroup.Name}
			printer.Append(line)
		}

		printer.Print(groupHeader)

		return nil
	},
}

var groupListSubgroupsCmd = &cobra.Command{
	Use:   "subgroups [group]",
	Short: "List all sub-groups of an accounting group in the system",
	Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		group := args[0]
		endpoint := fmt.Sprintf("%s/subgroups/%s", groupEndpoint, group)

		ret, err := client.Get(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		accts := make([]entities.AccountingGroupExtended, 0)
		codec.Decode(ret, &accts)

		for _, acct := range accts {
			line := []string{strconv.Itoa(acct.Group.ID), acct.Group.Name, acct.Group.Description, acct.Parent.Name, acct.ChargeToGroup.Name}
			printer.Append(line)
		}

		printer.Print(groupHeader)

		return nil
	},
}

var groupListTopCmd = &cobra.Command{
	Use:   "top",
	Short: "List all top-level accounting groups in the system",

	RunE: func(cmd *cobra.Command, args []string) error {
		endpoint := fmt.Sprintf("%s/top", groupEndpoint)
		ret, err := client.Get(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		accts := make([]entities.AccountingGroupExtended, 0)
		codec.Decode(ret, &accts)

		for _, acct := range accts {
			line := []string{strconv.Itoa(acct.Group.ID), acct.Group.Name, acct.Group.Description, acct.Parent.Name, acct.ChargeToGroup.Name}
			printer.Append(line)
		}

		printer.Print(groupHeader)

		return nil
	},
}

var groupGetCmd = &cobra.Command{
	Use:   "get [accounting group]",
	Short: "Get a specific accounting group",
	Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		group := args[0]

		endpoint := fmt.Sprintf("%s/%s", groupEndpoint, group)
		ret, err := client.Get(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		acct := entities.AccountingGroupExtended{}
		codec.Decode(ret, &acct)

		line := []string{strconv.Itoa(acct.Group.ID), acct.Group.Name, acct.Group.Description, acct.Parent.Name, acct.ChargeToGroup.Name}
		printer.Append(line)
		printer.Print(groupHeader)

		return nil
	},
}

var groupUpdateCmd = &cobra.Command{
	Use:   "update [id] [description|chargegroup] [value]",
	Short: "Update an accounting group",
	Long:  `Update an accounting group. You can only update the description or the charge group fields.`,
	Args:  cobra.ExactArgs(3),

	PreRunE: func(cmd *cobra.Command, args []string) error {
		if args[1] != "description" && args[1] != "chargegroup" {
			return errors.New("you can only update the description or the charge group fields")
		}

		return nil
	},

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]
		field := args[1]
		value := args[2]

		endpoint := fmt.Sprintf("%s/%s/%s", groupEndpoint, field, id)
		ret, err := client.Put(endpoint, nil, value)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

var groupDeleteCmd = &cobra.Command{
	Use:   "delete [group]",
	Short: "Remove an accounting group from the system",
	Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]

		endpoint := fmt.Sprintf("%s/%s", groupEndpoint, id)
		ret, err := client.Delete(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

func init() {
	grpupCmd.AddCommand(groupCreateCmd)
	grpupCmd.AddCommand(groupListCmd)
	grpupCmd.AddCommand(groupGetCmd)
	grpupCmd.AddCommand(groupUpdateCmd)
	grpupCmd.AddCommand(groupDeleteCmd)

	groupCreateCmd.AddCommand(groupCreateTopCmd)

	groupListCmd.AddCommand(groupListSubgroupsCmd)
	groupListCmd.AddCommand(groupListTopCmd)

	rootCmd.AddCommand(grpupCmd)
}
