package commands

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	entities "gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-entities"
	"strconv"
	"strings"
)

const chargeEndpoint = "chargegroup"

var chargeHeader = []string{"ID", "Name", "UUID", "Category", "Active", "Admins"}

type charge struct {
	Name     string `json:"name"`
	Category string `json:"category"`
}

var chargeCmd = &cobra.Command{
	Use:   "charge",
	Short: "Manage charge groups",
}

var chargeCreateCmd = &cobra.Command{
	Use:   "create [name] [category]",
	Short: "Create new charge groups",
	Args:  cobra.ExactArgs(2),

	RunE: func(cmd *cobra.Command, args []string) error {
		name := args[0]
		category := args[1]

		c := charge{
			Name:     name,
			Category: category,
		}

		data, _ := codec.Encode(c)
		ret, err := client.Post(chargeEndpoint, data)

		if err != nil {
			return handleError(ret, err)
		}

		charge := entities.ChargeGroup{}
		codec.Decode(ret, &charge)

		fmt.Println("New resource:")

		line := []string{strconv.Itoa(charge.ID), charge.Name, charge.UUID, charge.Category, strconv.FormatBool(charge.Active)}
		printer.Append(line)
		printer.Print(chargeHeader)

		return nil
	},
}

var chargeListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all charge groups in the system",

	RunE: func(cmd *cobra.Command, args []string) error {
		ret, err := client.Get(chargeEndpoint)

		if err != nil {
			return handleError(ret, err)
		}

		charges := make([]entities.ChargeGroup, 0)
		codec.Decode(ret, &charges)

		for _, charge := range charges {
			var adminsLine []string
			if len(charge.Admins) != 0 && len(charge.Admins[0].Email) != 0 {
				for _, ad := range charge.Admins {
					adminsLine = append(adminsLine, fmt.Sprintf("%s:%s", ad.Username, ad.Email))
				}
			}

			line := []string{
				strconv.Itoa(charge.ID), charge.Name,
				charge.UUID, charge.Category,
				strconv.FormatBool(charge.Active),
				strings.Join(adminsLine, " "),
			}
			printer.Append(line)
		}

		printer.Print(chargeHeader)

		return nil
	},
}

var chargeGetCmd = &cobra.Command{
	Use:   "get [group]",
	Short: "Get a specific charge group",
	Args:  cobra.ExactArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]

		endpoint := fmt.Sprintf("%s/%s", chargeEndpoint, id)
		ret, err := client.Get(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		charge := entities.ChargeGroup{}
		codec.Decode(ret, &charge)

		var adminsLine []string
		if len(charge.Admins) != 0 && len(charge.Admins[0].Email) != 0 {
			for _, ad := range charge.Admins {
				adminsLine = append(adminsLine, fmt.Sprintf("%s:%s", ad.Username, ad.Email))
			}
		}

		line := []string{
			strconv.Itoa(charge.ID), charge.Name,
			charge.UUID, charge.Category,
			strconv.FormatBool(charge.Active),
			strings.Join(adminsLine, " ")}
		printer.Append(line)
		printer.Print(chargeHeader)

		return nil
	},
}

var chargeUpdateCmd = &cobra.Command{
	Use:   "update [id] [field] [value]",
	Short: "Update a charge group",
	Long:  `Update a charge group. Only the 'active' field is updatable.`,
	Args:  cobra.ExactArgs(3),

	PreRunE: func(cmd *cobra.Command, args []string) error {
		if args[1] != "active" {
			return errors.New("only the 'active' field is updatable")
		}

		return nil
	},

	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]
		field := args[1]
		value := args[2]

		endpoint := fmt.Sprintf("%s/%s/%s", chargeEndpoint, field, id)
		ret, err := client.Put(endpoint, nil, value)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}

		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

func init() {
	chargeCmd.AddCommand(chargeCreateCmd)
	chargeCmd.AddCommand(chargeListCmd)
	chargeCmd.AddCommand(chargeGetCmd)
	chargeCmd.AddCommand(chargeUpdateCmd)

	rootCmd.AddCommand(chargeCmd)
}
