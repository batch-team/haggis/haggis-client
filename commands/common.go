package commands

import "errors"

var client Client
var codec Codec
var printer Printer

type successMessage struct {
	Message string `json:"message"`
}

type errorMessage struct {
	Error string `json:"error"`
}

func handleError(ret []byte, err error) error {
	if ret != nil {
		err := errorMessage{}
		codec.Decode(ret, &err)
		return errors.New(err.Error)
	}

	return err
}

//Client asbstracts a request client
type Client interface {
	Get(endpoint string) ([]byte, error)
	Post(endpoint string, data []byte) ([]byte, error)
	Put(endpoint string, data []byte, value string) ([]byte, error)
	Delete(endpoint string) ([]byte, error)
}

//Encoder abstracts an encoding facility
type Encoder interface {
	Encode(src interface{}) ([]byte, error)
}

//Decoder abstracts a decoding facility
type Decoder interface {
	Decode(src []byte, dst interface{}) error
}

//Codec abstracts an encode/decode facility
type Codec interface {
	Encoder
	Decoder
}

//Printer abstracts a printing facility
type Printer interface {
	Append(line []string)
	Print(header []string)
}
