package commands

import (
	"errors"
	"fmt"
	entities "gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-entities"
	"strconv"

	"github.com/spf13/cobra"
)

const resourceEndpoint = "poolgroup"

var resourceHeader = []string{"Pool ID", "Pool Name", "Group ID", "Group Name", "Quota", "Charge Role", "Pledge", "Charge As"}

var resourceCmd = &cobra.Command{
	Use:   "resource",
	Short: "Manage pool to group mappings",
}

type resource struct {
	Pool       int       `json:"pool"`
	Group      int       `json:"group"`
	Role       string    `json:"chargerole,omitempty"`
	Properties int       `json:"chargeproperties"`
	Resources  resources `json:"resources"`
}

type resources struct {
	Quota    int  `json:"quota"`
	Weight   int  `json:"weight"`
	Priority int  `json:"priority"`
	Surplus  bool `json:"surplus"`
}

type share struct {
	ID    int `json:"id"`
	Quota int `json:"quota"`
}

var resourceCreateCmd = &cobra.Command{
	Use:   "create [pool] [group] [chargerole] [chargeproperties] [quota]",
	Short: "Create a new pool to group mapping",
	Args:  cobra.ExactArgs(5),

	RunE: func(cmd *cobra.Command, args []string) error {
		pool, _ := strconv.Atoi(args[0])
		group, _ := strconv.Atoi(args[1])
		role := args[2]
		properties, _ := strconv.Atoi(args[3])
		quota, _ := strconv.Atoi(args[4])

		resources := resources{
			Quota:    quota,
			Weight:   0,
			Priority: 10000,
			Surplus:  true,
		}

		pg := resource{
			Pool:       pool,
			Group:      group,
			Role:       role,
			Properties: properties,
			Resources:  resources,
		}

		data, _ := codec.Encode(pg)
		ret, err := client.Post(resourceEndpoint, data)

		if err != nil {
			return handleError(ret, err)
		}

		resource := entities.PoolGroup{}
		codec.Decode(ret, &resource)

		fmt.Println("New resource:")

		line := []string{strconv.Itoa(resource.Pool), "n/a", strconv.Itoa(resource.Group), "n/a", strconv.Itoa(resource.Resources.Quota), resource.ChargeRole, "n/a", "n/a"}
		printer.Append(line)
		printer.Print(resourceHeader)

		return nil
	},
}

var resourceListCmd = &cobra.Command{
	Use:   "list [pool]",
	Short: "List all resource pool to group mappings in the system",
	Args:  cobra.MaximumNArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		var endpoint string
		endpoint = resourceEndpoint

		if len(args) == 1 {
			pool := args[0]
			endpoint = fmt.Sprintf("%s/%s", resourceEndpoint, pool)
		}

		ret, err := client.Get(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		resources := make([]entities.PoolGroupExtended, 0)
		codec.Decode(ret, &resources)

		for _, resource := range resources {
			line := []string{strconv.Itoa(resource.Pool.ID), resource.Pool.Name,
				strconv.Itoa(resource.Group.ID), resource.Group.Name, strconv.Itoa(resource.PoolGroup.Resources.Quota), resource.PoolGroup.ChargeRole,
				strconv.FormatBool(resource.ChargeProperties.Pledge), resource.ChargeProperties.Type}
			printer.Append(line)
		}

		printer.Print(resourceHeader)

		return nil
	},
}

var resourceGetCmd = &cobra.Command{
	Use:   "get [pool] [group]",
	Short: "Get a specific pool to group mapping",
	Args:  cobra.ExactArgs(2),

	RunE: func(cmd *cobra.Command, args []string) error {
		pool := args[0]
		group := args[1]

		endpoint := fmt.Sprintf("%s/%s/%s", resourceEndpoint, pool, group)
		ret, err := client.Get(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		resource := entities.PoolGroupExtended{}
		codec.Decode(ret, &resource)

		line := []string{strconv.Itoa(resource.Pool.ID), resource.Pool.Name,
			strconv.Itoa(resource.Group.ID), resource.Group.Name, strconv.Itoa(resource.PoolGroup.Resources.Quota), resource.PoolGroup.ChargeRole,
			strconv.FormatBool(resource.ChargeProperties.Pledge), resource.ChargeProperties.Type}

		printer.Append(line)
		printer.Print(resourceHeader)

		return nil
	},
}

var resourceUpdateCmd = &cobra.Command{
	Use:   "update [pool] [group] [field] [value]",
	Short: "Update an pool to group mapping",
	Long:  `Update an accounting group. You can only update the description or the charge group fields.`,
	Args:  cobra.ExactArgs(4),

	PreRunE: func(cmd *cobra.Command, args []string) error {
		if args[2] != "quota" && args[2] != "chargerole" && args[2] != "chargeproperties" {
			return errors.New("you can only update the quota field")
		}

		return nil
	},

	RunE: func(cmd *cobra.Command, args []string) error {
		var endpoint string

		pool := args[0]
		group := args[1]
		field := args[2]
		value := args[3]

		endpoint = fmt.Sprintf("%s/%s/%s/%s", resourceEndpoint, field, pool, group)

		if field == "quota" {
			endpoint = fmt.Sprintf("%s/resources/%s/%s/%s", resourceEndpoint, field, pool, group)
		}

		ret, err := client.Put(endpoint, nil, value)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

var resourceRebalanceCmd = &cobra.Command{
	Use:   "rebalance [pool] [group]",
	Short: "Update an pool to group mapping",
	//Long:  `Update an accounting group. You can only update the description or the charge group fields.`,
	Args: cobra.ExactArgs(2),

	RunE: func(cmd *cobra.Command, args []string) error {
		balance, _ := cmd.Flags().GetStringToInt("balance")

		pool := args[0]
		group := args[1]

		shares := make([]share, 0)

		for k, v := range balance {
			id, _ := strconv.Atoi(k)

			share := share{
				ID:    id,
				Quota: v,
			}

			shares = append(shares, share)
		}

		data, _ := codec.Encode(shares)
		endpoint := fmt.Sprintf("%s/resources/quota/balance/%s/%s", resourceEndpoint, pool, group)
		ret, err := client.Put(endpoint, data, "")

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

var resourceDeleteCmd = &cobra.Command{
	Use:   "delete [pool] [group]",
	Short: "Remove an pool to group mapping from the system",
	Args:  cobra.ExactArgs(2),

	RunE: func(cmd *cobra.Command, args []string) error {
		pool := args[0]
		group := args[1]

		endpoint := fmt.Sprintf("%s/%s/%s", resourceEndpoint, pool, group)
		ret, err := client.Delete(endpoint)

		if err != nil {
			return handleError(ret, err)
		}

		msg := successMessage{}
		codec.Decode(ret, &msg)

		fmt.Println(msg.Message)

		return nil
	},
}

func init() {
	resourceCmd.AddCommand(resourceCreateCmd)
	resourceCmd.AddCommand(resourceListCmd)
	resourceCmd.AddCommand(resourceGetCmd)
	resourceCmd.AddCommand(resourceUpdateCmd)
	resourceCmd.AddCommand(resourceRebalanceCmd)
	resourceCmd.AddCommand(resourceDeleteCmd)

	resourceRebalanceCmd.Flags().StringToInt("balance", nil, "this is how you use it")

	rootCmd.AddCommand(resourceCmd)
}
