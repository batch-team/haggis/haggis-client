SPECFILE = haggis-client.spec
FILES = haggis-client

rpmtopdir := $(shell rpm --eval %_topdir)
rpmbuild  := $(shell [ -x /usr/bin/rpmbuild ] && echo rpmbuild || echo rpm)

PKGNAME = $(shell grep -s '^Name:'    $(SPECFILE) | sed -e 's/Name: *//')
PKGVERS = $(shell grep -s '^Version:' $(SPECFILE) | sed -e 's/Version: *//')
PKGID=$(PKGNAME)-$(PKGVERS)
TARFILE=$(PKGID).tar.gz

clean:
	rm -rf $(TARFILE)
	rm -rf $(FILES)
	rm -rf $(PKGID)

all:	
	go build -o $(FILES) cmd/$(FILES)/main.go
	mkdir $(PKGID)
	cp -f $(FILES) $(PKGID)
	tar cvzf $(TARFILE) $(PKGID)

sources: srpm 

srpm: all
	rpmbuild -bs --define '_sourcedir ${PWD}' $(SPECFILE)

rpm: all
	rpmbuild -ba --define '_sourcedir $(PWD)' ${SPECFILE}

scratch6:
	koji build batch6 --nowait --scratch  git+ssh://git@gitlab.cern.ch:7999/batch-team/haggis-client.git#$(shell git rev-parse HEAD)
			
scratch7:
	koji build batch7 --nowait --scratch  git+ssh://git@gitlab.cern.ch:7999/batch-team/haggis-client.git#$(shell git rev-parse HEAD)

scratch8:
	koji build batch8 --nowait --scratch  git+ssh://git@gitlab.cern.ch:7999/batch-team/haggis-client.git#$(shell git rev-parse HEAD)

build6:
	koji build batch6 --nowait git+ssh://git@gitlab.cern.ch:7999/batch-team/haggis-client.git#$(shell git rev-parse HEAD)

build7:
	koji build batch7 --nowait git+ssh://git@gitlab.cern.ch:7999/batch-team/haggis-client.git#$(shell git rev-parse HEAD)

build8:
	koji build batch8 --nowait git+ssh://git@gitlab.cern.ch:7999/batch-team/haggis-client.git#$(shell git rev-parse HEAD)
