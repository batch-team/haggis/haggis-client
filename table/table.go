package table

import (
	"io"

	"github.com/olekukonko/tablewriter"
)

//Table implements a table printer
type Table struct {
	table *tablewriter.Table
}

//New returns a new table
func New(writer io.Writer) *Table {
	table := tablewriter.NewWriter(writer)

	return &Table{
		table: table,
	}
}

//Append adds a new line to the table
func (t *Table) Append(line []string) {
	t.table.Append(line)
}

//Print displays the constructed table
func (t *Table) Print(header []string) {
	t.table.SetAutoFormatHeaders(false)
	t.table.SetAlignment(tablewriter.ALIGN_LEFT)
	t.table.SetHeader(header)

	t.table.Render()
}
