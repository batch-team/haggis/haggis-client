package main

import (
	"gitlab.cern.ch/batch-team/haggis-client/commands"
	"os"
)

func main() {
	if _, err := os.Stat("/etc/krb5_flat.conf"); err == nil {
		os.Setenv("KRB5_CONFIG", "/etc/krb5_flat.conf")
	}
	commands.Execute()
}
