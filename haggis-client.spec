Name:           haggis-client
Version:        0.2.6
Release:        1%{dist}
Summary:        Haggis CLI client.

Group:          System Environment/Base
License:        Mozilla Public License 2.0
URL:            http://cern.ch/batchdocs
BuildRoot:      %{_tmppath}/%{name}-%{version}-root
BuildRequires: 	golang
Source:         %{name}-%{version}.tar.gz

%define debug_package %{nil}
%description

%prep
%setup -q

%build
mkdir -p inst
go build -mod=vendor -o inst/haggis cmd/haggis-client/main.go

%install
install -d %{buildroot}%{_bindir}
install -p -m 0755 %{_builddir}/%{name}-%{version}/inst/haggis %{buildroot}%{_bindir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/haggis



%changelog
* Thu Feb 01 2024 Ben Jones <b.jones@cern.ch> 0.2.6-1
- Update deps
* Mon Nov 20 2023 Ben Jones <b.jones@cern.ch> 0.2.5-1
- Use new negotiate, default to different KRB5CONFIG
* Fri Jun 18 2021 Ben Jones <b.jones@cern.ch> 0.2.4-1
- removes warning in output
- rpmci
- build support for 8
* Wed Aug 14 2019 Paulo Alexandre Canilho <paulo.canilho@cern.ch> 0.2.3-3
- Adding the admins column [user:email] to the result of the [charge] [list/get] commands
- Improved the documentation for the help menu of the [compute] command to better describe the possible use-cases
* Tue Aug 13 2019 Paulo Alexandre Canilho <paulo.canilho@cern.ch> 0.2.3-2
- Replaced the public GSSAPI library for an internal GitLab repo version
* Fri Aug 09 2019 Paulo Alexandre Canilho <paulo.canilho@cern.ch> 0.2.3-1
- Fix the compute get command single return issue [BBC-2428]
* Tue Jun 11 2019 Nikolaos Petros Triantafyllidis <nikolaos.petros.triantafyllidis@cern.ch> 0.2.2-1
- Added resource default values 
* Wed Mar 27 2019 Nikolaos Petros Triantafyllidis <nikolaos.petros.triantafyllidis@cern.ch> 0.2.1-1
- Added reverse compute group lookup functionality
* Tue Feb 26 2019 Nikolaos Petros Triantafyllidis <nikolaos.petros.triantafyllidis@cern.ch> 0.2.0-1
- Port to new backend and complete rewrite
* Tue Feb 26 2019 Nikolaos Petros Triantafyllidis <nikolaos.petros.triantafyllidis@cern.ch> 0.1.2-1
- Changed the name of some commands
* Tue Feb 05 2019 Nikolaos Petros Triantafyllidis <nikolaos.petros.triantafyllidis@cern.ch> 0.1.1-1
- Fixed update methods
* Mon Feb 04 2019 Nikolaos Petros Triantafyllidis <nikolaos.petros.triantafyllidis@cern.ch> 0.1.0-1
- First packaging.
