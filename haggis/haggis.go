package haggis

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.cern.ch/batch-team/negotiate"
	"io"
	"net/http"
)

// Haggis implements a web client that performs requests to the haggis back end server.
type Haggis struct {
	scheme    string
	host      string
	port      string
	user      string
	negotiate bool
}

// New returns a new Haggis
func New(scheme string, host string, port string, user string, negotiate bool) Haggis {
	return Haggis{
		scheme:    scheme,
		host:      host,
		port:      port,
		user:      user,
		negotiate: negotiate,
	}
}

// Get sends an HTTP GET request.
func (h Haggis) Get(endpoint string) ([]byte, error) {
	fmt.Println(h.prepareRequest(endpoint))
	resp, err := negotiate.KerberosHTTPRequest(http.MethodGet, h.prepareRequest(endpoint), nil, negotiate.NoAliasDeref())

	if err != nil {
		return nil, err
	}

	return renderOutput(resp)
}

// Post sends an HTTP POST request.
func (h Haggis) Post(endpoint string, data []byte) ([]byte, error) {
	body := io.NopCloser(bytes.NewReader(data))
	resp, err := negotiate.KerberosHTTPRequest(http.MethodPost, h.prepareRequest(endpoint), body, negotiate.NoAliasDeref())

	if err != nil {
		return nil, err
	}

	return renderOutput(resp)
}

// Put sends an HTTP PUT request.
func (h Haggis) Put(endpoint string, data []byte, value string) ([]byte, error) {
	body := io.NopCloser(bytes.NewReader(data))
	resp, err := negotiate.KerberosHTTPRequest(http.MethodPut, h.prepareRequest(endpoint), body, negotiate.NoAliasDeref())

	if err != nil {
		return nil, err
	}

	return renderOutput(resp)
}

// Delete sends an HTTP DELETE request.
func (h Haggis) Delete(endpoint string) ([]byte, error) {
	resp, err := negotiate.KerberosHTTPRequest(http.MethodDelete, h.prepareRequest(endpoint), nil, negotiate.NoAliasDeref())

	if err != nil {
		return nil, err
	}

	return renderOutput(resp)
}

func (h Haggis) prepareRequest(endpoint string) string {
	return fmt.Sprintf("%s://haggis02.cern.ch:%s/%s", h.scheme, h.port, endpoint)
}

func renderOutput(resp *http.Response) ([]byte, error) {
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	if err := handleStatusCode(resp.StatusCode); err != nil {
		return body, err
	}

	return body, nil
}

func handleStatusCode(status int) error {
	switch status {
	case 200:
		return nil
	default:
		//We will let the error handler display the error provided by the server.
		//Thus the error message here can be left blank.
		return errors.New("")
	}
}
