package json

import (
	"encoding/json"
)

//Codec wraps an encoding/json codec
type Codec struct{}

//New returns a new Codec
func New() Codec {
	return Codec{}
}

//Decode wraps the encoding/json Unmarshal method
func (c Codec) Decode(src []byte, dst interface{}) error {
	return json.Unmarshal(src, dst)
}

//Encode wraps the encoding/json Marshal method
func (c Codec) Encode(src interface{}) ([]byte, error) {
	return json.Marshal(src)
}
